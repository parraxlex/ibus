from django.db import models
from django.utils import timezone
from django.contrib.auth.models import ( AbstractBaseUser, BaseUserManager, PermissionsMixin)
# Create your models here.

class sTarjeta(models.Model):
    monto = models.FloatField(default=0)
    fechaExpiracion = models.DateField()
    is_active = models.BooleanField(default=True)



class UserManager(BaseUserManager):
    def create_user(self, CURP, password):
        if not CURP:
            raise ValueError("ENTER AN EMAIL BUDDY")
        user = self.model(
            CURP=CURP
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, CURP, password ):
        user = self.model(
            CURP=CURP
        )
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):

    CHOICE_SEXO = (
        ('M', 'Masculino'),
        ('F', 'Femenino')
    )
    CHOICE_NACIONALIDAD = (
        ('M', 'Mexicana'),
        ('E', 'Extrajera')
    )


    CURP = models.CharField(max_length=18, unique=True)
    nombre = models.CharField(max_length=100)
    apellidoPaterno = models.CharField(max_length=100)
    apellidoMaterno = models.CharField(max_length=100)
    sexo = models.CharField(max_length=1, choices=CHOICE_SEXO)
    fechaNacimiento = models.DateField(null=True)
    nacionalidad = models.CharField(max_length=1, choices=CHOICE_NACIONALIDAD)
    correoElectronico = models.CharField(
        max_length=150, unique=True)
    telefono = models.CharField(max_length=15, null=True)
    celular = models.CharField(max_length=15, unique=True, null=True)

    # default user model
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    

    # default create user
    objects = UserManager()
    USERNAME_FIELD = "CURP"
    REQUIRED_FIELDS = []


