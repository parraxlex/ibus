from django.db import models

class sVehiculo(models.Model):
    aModelo = models.CharField(max_length=50)
    aMarca = models.CharField(max_length=50)
    aMatricula = models.CharField(max_length=25, unique=True)
    aNumeroEconomico = models.CharField(max_length=25, unique=True)

    aCreated_at = models.DateTimeField(auto_now_add=True)
    aUpdated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'sVehiculo'

