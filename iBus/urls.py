from django.urls import path, include
from . import views
from iBus.views import *

urlpatterns = [
    path('', Index.as_view(), name='index'),
    #path('rutas', Rutas.as_view(), name='rutas'),
    # Auth
    path('ingresar',Login.as_view(), name="login"),
    path('registrar', Register.as_view(), name="register"),
    path('salir', Logout.as_view(), name='logout'),
    
    #------------- Aplicacion ---------------#
    path('rutas', Rutas.as_view(), name='rutas'),

    #------------- Usuario ---------------#
    path('configuracion/usuario', Profile.as_view(), name='configuracion.perfil'),


    #------------- Configuracion ---------------#
    path('configuracion/rutas', RutaConfig.as_view(), name='configuracion.rutas'),
    path('configuracion/estaciones', EstacionConfig.as_view(), name='configuracion.estaciones'),
    path('configuracion/conductores', ConductorConfig.as_view(), name='configuracion.conductores'),
    path('configuracion/vehiculos', VehiculoConfig.as_view(), name='configuracion.vehiculos'),

]