from django.contrib.auth import login, authenticate
from django.shortcuts import render
from django.views.generic import CreateView
from iBus.models import sRuta, sGrupoRuta, sTraza, User
from django.http import JsonResponse


class Profile(CreateView):
    template_name = "user/perfil.html"

    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, 'iBus/errors/403.html')
        else:
            pFlag = False
            return render(request, self.template_name)
    
    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, '403.html')
        else:
            if request.POST.get('operation') == 'read':
                cntx = {
                    'nombre': request.user.nombre,
                    'apellidoPaterno': request.user.apellidoPaterno,
                    'apellidoMaterno': request.user.apellidoMaterno,
                    'CURP': request.user.CURP,
                    'sexo': request.user.sexo,
                    'fechaNacimiento': request.user.fechaNacimiento,
                    'nacionalidad': request.user.nacionalidad,
                    'correoElectronico': request.user.correoElectronico,
                    'telefono': request.user.telefono,
                    'celular':request.user.celular
                }
                return JsonResponse(cntx)

            if request.POST.get('operation') == 'update':
                try:
                    user = User.objects.get(CURP=request.user.CURP)
                    print (user)
                    user.telefono = request.POST.get('telefono')
                    user.celular = request.POST.get('celular')
                    user.correoElectronico = request.POST.get('correoElectronico')
                    user.save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})
