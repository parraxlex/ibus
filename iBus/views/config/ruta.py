from django.contrib.auth import login, authenticate
from django.shortcuts import render
from django.views.generic import CreateView
from iBus.models import sRuta, sGrupoRuta, sTraza, sEstacionRuta, sEstacion
from django.http import JsonResponse


class RutaConfig(CreateView):
    template_name = "config/ruta.html"

    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, 'iBus/errors/403.html')
        else:
            pFlag = False
            return render(request, self.template_name)
    
    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, '403.html')
        else:
            if request.POST.get('operation') == 'read':
                rutasQuery = sRuta.objects.values(
                    'id', 'aNombre', 'aDescripcion')
                rutas = []
                for ruta in rutasQuery:
                    rutas.append({
                        'id': ruta['id'],
                        'aNombre': ruta['aNombre'],
                        'aDescripcion':  ruta['aDescripcion']
                    })
                cntx = {
                    'rutas': rutas
                }
                return JsonResponse(cntx)
            if request.POST.get('operation') == 'select':
                rutaQuery = sRuta.objects.filter(id=request.POST.get('id')).values('id', 'aNombre', 'aDescripcion').first()
                cntx = {
                    'id' : rutaQuery['id'],
                    'aNombre' : rutaQuery['aNombre'],
                    'aDescripcion': rutaQuery['aDescripcion']
                }
                return JsonResponse(cntx)
            
            if request.POST.get('operation') == 'update':
                try:
                    ruta = sRuta.objects.get(id=request.POST.get('id'))
                    ruta.aNombre = request.POST.get('aNombre')
                    ruta.aDescripcion = request.POST.get('aDescripcion')
                    ruta.save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            if request.POST.get('operation') == 'create':
                try:
                    sRuta(
                        aNombre=request.POST.get('aNombre'),
                        aDescripcion=request.POST.get('aDescripcion')
                    ).save()
                    RutaIDLast = sRuta.objects.latest('id')
                    sGrupoRuta(
                        RutaID=RutaIDLast,
                        aTipo='A'
                    ).save()
                    sGrupoRuta(
                        RutaID=RutaIDLast,
                        aTipo='B'
                    ).save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            if request.POST.get('operation') == 'delete':
                try:
                    sRuta.objects.filter(id=request.POST.get('id')).delete()
                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})



            if request.POST.get('operation') == 'selectTraza':
                grupoRutaQuery = sGrupoRuta.objects.filter(RutaID=request.POST.get('id'), aTipo=request.POST.get('aTipo')).values('id').first()
                trazasQuery = sTraza.objects.filter(GrupoRutaID=grupoRutaQuery['id']).values('id','aLongitud', 'aLatitud')
                trazas = []
                for traza in trazasQuery:
                    trazas.append({
                        'id': traza['id'],
                        'aLongitud': traza['aLongitud'],
                        'aLatitud': traza['aLatitud']
                    })

                cntx = {
                    'trazas': trazas,
                    'GrupoRutaID': grupoRutaQuery['id']
                }
                return JsonResponse(cntx)

            if request.POST.get('operation') == 'updateTraza':
                try:
                    sTraza(
                        aLatitud = request.POST.get('aLatitud'),
                        aLongitud = request.POST.get('aLongitud'),
                        GrupoRutaID = sGrupoRuta.objects.get(id=request.POST.get('GrupoRutaID')),
                    ).save()

                    return JsonResponse({'status': '200'})
                except:
                    return JsonResponse({'status': '500'})

            if request.POST.get('operation') == 'selectEstacion':
                grupoRutaQuery = sGrupoRuta.objects.filter(RutaID=request.POST.get('id'), aTipo=request.POST.get('aTipo')).values('id').first()
                estacionesGrupoQuery = sEstacionRuta.objects.filter(GrupoRutaID=grupoRutaQuery['id']).all()
                estacionesGrupo = []
                for estacion in estacionesGrupoQuery:
                    estacionesGrupo.append({
                        'id': estacion.EstacionID.id,
                        'aNombre' :estacion.EstacionID.aNombre,
                        'aLatitud': estacion.EstacionID.aLatitud,
                        'aLongitud': estacion.EstacionID.aLongitud
                    })
                estaciones = []
                estacionesQuery = sEstacion.objects.values('id','aNombre')
                for estacion in estacionesQuery:
                    estaciones.append({
                        'id': estacion['id'],
                        'aNombre': estacion['aNombre']
                    })
                cntx = {
                    'estaciones': estaciones,
                    'estacionesGrupo': estacionesGrupo
                }
                return JsonResponse(cntx)

