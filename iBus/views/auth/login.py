from django.contrib.auth import login, authenticate
from django.shortcuts import render , redirect
from django.views.generic import View
from django.contrib.auth import authenticate


class Login(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('index')
        else:
            return render(request, 'iBus/login.html')

    def post(self, request):
        if request.method == 'POST':
            user = authenticate(CURP=request.POST.get('CURP'), password=request.POST.get('password'))
            login(request, user)
            return render(request, 'iBus/index.html')