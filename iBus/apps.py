from django.apps import AppConfig


class IbusConfig(AppConfig):
    name = 'iBus'
